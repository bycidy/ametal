/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief HC32 SPI SLV DMA 用户配置文件
 * \sa am_hwconf_hc32_spi_slv_dma.c
 *
 * \internal
 * \par Modification history
 * - 1.00 20-04-01  cds, first implementation
 * \endinternal
 */

#include "am_hc32.h"
#include "ametal.h"
#include "am_gpio.h"
#include "am_clk.h"
#include "hw/amhw_hc32_spi.h"
#include "am_hc32_spi_slv_dma.h"
/**
 * \addtogroup am_if_src_hwconf_hc32_spi_slv_dma
 * \copydoc am_hwconf_hc32_spi_slv_dma.c
 * @{
 */
#define __SPI1_PIN_NSS    PIOB_12

/** \brief SPI1 平台初始化 */
static void __hc32_plfm_spi1_slv_dma_init (void)
{
    amhw_hc32_gpio_sup_spi1_ssn_set(HC32_GPIO0,AMHW_HC32_GPIO_SUP_EXTCLK_SSN01_PB12);
    am_gpio_pin_cfg(PIOB_13, PIOB_13_SPI1_SCK  | AM_GPIO_INPUT);
    am_gpio_pin_cfg(PIOB_15, PIOB_15_SPI1_MOSI | AM_GPIO_INPUT);
    am_gpio_pin_cfg(PIOB_14, PIOB_14_SPI1_MISO | PIOB_14_OUT_PP);

    am_clk_enable(CLK_SPI1);
}

/** \brief 解除 SPI1 平台初始化 */
static void __hc32_plfm_spi1_slv_dma_deinit (void)
{
    am_gpio_pin_cfg(PIOB_13, PIOB_10_INPUT_PU);
    am_gpio_pin_cfg(PIOB_15, PIOB_15_INPUT_PU);
    am_gpio_pin_cfg(PIOB_14, PIOB_14_INPUT_PU);

    am_clk_disable(CLK_SPI1);
}

/**
 * \brief SPI1 设备信息
 */
static const struct am_hc32_spi_slv_dma_devinfo  __g_spi1_slv_dma_devinfo = {
    HC32_SPI1_BASE,                 /**< \brief SPI1寄存器指针 */
    CLK_SPI1,                           /**< \brief 时钟ID号 */
    1,                                  /**< \brief SPI设备ID号 */
    1,                                  /**< \brief DMA设备ID号 */
    DMA_CHAN_1,                         /**< \brief DMA发送通道号 */
    DMA_CHAN_2,                         /**< \brief DMA接收通道号 */
    __SPI1_PIN_NSS,                     /**< \brief SPI1的NSS引脚，只能使用默认的NSS引脚*/
    __hc32_plfm_spi1_slv_dma_init,  /**< \brief SPI1平台初始化函数 */
    __hc32_plfm_spi1_slv_dma_deinit /**< \brief SPI1平台解初始化函数 */
};

/** \brief SPI1 设备实例 */
static am_hc32_spi_slv_dma_dev_t __g_spi1_slv_dma_dev;


/** \brief SPI1 实例初始化，获得SPI标准服务句柄 */
am_spi_slv_handle_t am_hc32_spi1_slv_dma_inst_init (void)
{
    return am_hc32_spi_slv_dma_init(&__g_spi1_slv_dma_dev,
                                        &__g_spi1_slv_dma_devinfo);
}

/** \brief SPI1 实例解初始化 */
void am_hc32_spi1_slv_dma_inst_deinit (am_spi_slv_handle_t handle)
{
    am_hc32_spi_slv_dma_deinit(handle);
}


/**
 * @}
 */

/* end of file */
