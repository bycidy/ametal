/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief NEOWAY_N58
 *
 * \internal
 * \par Modification History
 * - 1.00 20-05-27  tee, first implementation.
 * \endinternal
 */

#ifndef __AM_NEOWAY_N58_H
#define __AM_NEOWAY_N58_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ametal.h"
#include "am_types.h"
#include "am_uart_rngbuf.h"

/**
 * @addtogroup am_if_neoway_n58
 * @copydoc am_neoway_n58.h
 * @{
 */

/* module */
#define    NEOWAY_2G_MODULE   0
#define    NEOWAY_4G_MODULE   1

/* init_option */
#define    NORMAL_TCP_UNTRAN_CLIENT   0
#define    NORMAL_TCP_UNTRAN_SERVICE  1
#define    NORMAL_TCP_TRAN_CLIENT     2
#define    NATION_TCP_UNTRAN_CLIENT   3
#define    NATION_TCP_UNTRAN_SERVICE  4
#define    NATION_TCP_TRAN_CLIENT     5
#define    ENTERNAL_TCP_APP           6

#define    TCPLISTEN_PORT             6800
#define    TCPLISTEN_OVER_TIME        10    //单位：秒

#define    TCPSENDS_LENGTH            10
#define    TCPSENDS_DATA              "1234567890"

#define    TCP_IP                     "192.168.1.2"


/* 设置PPP拨号参数 */
void set_ppp_dial_parameters();

/* 关闭服务器侦听 */
int close_server_listening(am_uart_rngbuf_handle_t uart_handle, int establish);

/* 建立服务器端侦听 */
int establish_server_listening(am_uart_rngbuf_handle_t uart_handle);

/* 发送数据 */
int neoway_send_data(am_uart_rngbuf_handle_t uart_handle, 
                            const uint8_t           *p_txbuf,
                            uint32_t                 nbytes);

/* 去附着网络 */
int attached_to_the_network(am_uart_rngbuf_handle_t uart_handle);

/*  标准 TCP 非透传服务器端 */
int am_normal_tcp_untran_service(am_uart_rngbuf_handle_t uart_handle);

/* P10 */                            
int am_neoway_n58_tcp_communicate(am_uart_rngbuf_handle_t uart_handle,
                                                  uint8_t socket_id, 
                                                    char *send_data);

/* P23 */
int am_neoway_n58_tcp_Penetrate(am_uart_rngbuf_handle_t uart_handle);

/* 外部协议栈 TCP 应用 */
int enternal_tcp_app_init(am_uart_rngbuf_handle_t uart_handle);

/* 进入外部协议栈 */
int enter_external_protocol_stack(am_uart_rngbuf_handle_t uart_handle);
/** 
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __AM_NEOWAY_N58_H */

/* end of file */
