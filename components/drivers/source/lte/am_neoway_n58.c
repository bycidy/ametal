/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief NEOWAY_N58 driver implement
 *
 * \internal
 * \par Modification History
 * - 1.00 20-12-30  zjr, first implementation.
 * \endinternal
 */
#include "ametal.h"
#include "am_neoway_n58.h"
#include "am_gpio.h"
#include "am_int.h"
#include "am_uart.h"
#include "am_uart_rngbuf.h"
#include "am_vdebug.h"
#include "am_int.h"
#include "am_wait.h"
#include "am_delay.h"
#include "am_list.h"
#include "string.h"

/*******************************************************************************
    type defines
*******************************************************************************/

#define RESTART_NEOWAY_N58      while(1);/* 重启模块指令 */

#define SEND_COMMAND_TWO(a,b,c,d,e)   if(send_recv_with_decide2(a,b,c,d,e) != AM_OK)      \
                                    RESTART_NEOWAY_N58;

#define SEND_COMMAND(a,b,c,d)                                                  \
                                send_recv_with_decide(a,b,c,d)

#define SEND_COMMAND_WITH_RESTART(a,b,c,d)                                     \
                                if(send_recv_with_decide(a,b,c,d) != AM_OK) {  \
                                    RESTART_NEOWAY_N58;                        \
                                    return AM_ERROR;                           \
                                }

/* 查询数据发送情况 */
static int query_data_transmission(am_uart_rngbuf_handle_t uart_handle);

/* 查询链路状态 */
static int query_link_status(am_uart_rngbuf_handle_t uart_handle);

/* 等待接收客户端连接请求 */
static int waiting_receiving_client_connection(am_uart_rngbuf_handle_t uart_handle);

/* 查询PPP连接建立情况 */
static int query_the_ppp_connection(am_uart_rngbuf_handle_t uart_handle);

/* 进行PPP拨号 */
static int perform_ppp_dialing(am_uart_rngbuf_handle_t uart_handle);


int AcceptSocket = 0;
/*******************************************************************************
    global variables
*******************************************************************************/
/*
 * uart_handle    : 具有环形缓冲区的串口操作句柄，串口超时1s
 * *send_data     : 需要发送的字符串
 * *recv_data    : 对接收的字符串进行判断
 *  repeat_num   : 重复次数，
 */
static int send_recv_with_decide(am_uart_rngbuf_handle_t uart_handle,
                                 char                   *send_data, 
                                 char                   *recv_data, 
                                 int                     repeat_num)
{
    int null_times = 0;
    while(repeat_num--) {
        
        int send_data_num = am_uart_rngbuf_send(uart_handle, 
                                               (uint8_t *)send_data, 
                                                strlen(send_data));
        
        if(send_data_num == strlen(send_data)) {
            char recv_buff[32] = {0};
            am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 32);
            
            if(recv_buff[0] == 0) {
                null_times++;
                if(null_times == 3) {
                    return AM_ERROR;
                }
            }
            
            if(strstr((const char *)recv_data,"EVERY") != NULL) {
                if(recv_buff[0] != 0) {
                    return AM_OK;
                }
            } else if(strstr((const char *)recv_data,"/") != NULL) {
                char *recv_data0 = recv_data;
                char *recv_data1 = strstr((const char *)recv_data,"/") + 1;
                *(recv_data1 - 1) = 0;
                
                if((strstr(recv_buff, recv_data0) != NULL) || 
                   (strstr(recv_buff, recv_data1) != NULL)) {
                    return AM_OK;
                }
            } else if(strstr(recv_buff, recv_data) != NULL) {
                return AM_OK;
            }
            
        } else {
            return AM_ERROR;
        }
    }
    return AM_ERROR;
}


static int am_neoway_init(am_uart_rngbuf_handle_t uart_handle, int module, int init_option)
{
    int times = 0;
    int null_num = 0;
    
    /* 1&2:上电开机后延时 */
    if(module == NEOWAY_2G_MODULE) {
        am_mdelay(3000);
    } else if (module == NEOWAY_4G_MODULE) {
        am_mdelay(10000);
    } else {
        return -AM_EINVAL;                                  /* 无效参数 */
    }
    
    uart_handle->timeout_ms = 1000;
    /* 3: 检查串口是否已通 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT\r\n", "OK", 3);
    /* 4: 检查串口是否已通 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CGSN\r\n", "EVERY", 3);
    /* 5: 获取模块固件版本 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CMGR\r\n", "EVERY", 3);
    /* 6: 检查SIM卡状态 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CPIN?\r\n", "READY", 15);
    /* 7: 获取SIM卡IMSI */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CIMI\r\n", "OK", 3);
    /* 8: 查询信号强度 */
    times = 40;
    null_num = 3;
    while(times--) {
        int send_data_num = am_uart_rngbuf_send(uart_handle, (uint8_t*)"AT+CSQ\r\n", sizeof("AT+CSQ\r\n"));
        if(send_data_num == sizeof("AT+CSQ\r\n")) {
            char recv_buff[32] = {0};
            am_uart_rngbuf_receive(uart_handle, (uint8_t*)recv_buff, 32);
            if(recv_buff[0] == 0) {                         /* 无返回 */
                if(--null_num == 0) {
                    RESTART_NEOWAY_N58;                     /* 重启模块 */
                    return AM_ERROR;
                }
            } else if(strstr(recv_buff, "+CSQ:") != NULL) {
                char* num_sta = recv_buff + 6;
                char* num_end = strstr(recv_buff, ",");
                
                int signal = strtoul(num_sta, &num_end, 10);
                if((signal >= 12) && (signal <= 31)) {
                    break;
                }
                if(signal == 99) {
                    continue;
                }
            }
        }
    }
    if(times == 0) {
        RESTART_NEOWAY_N58;                     /* 重启模块 */
        return AM_ERROR;
    }
    
    /* 9: 设置主动上报网络注册小区信息 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CREG=2\r\n", "OK", 3);
    /* 查询网络注册情况 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CREG?\r\n", "2,1/2,5", 40);

    if(init_option != ENTERNAL_TCP_APP) {
        /* 设置禁止上报网络注册小区信息 */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CREG=0\r\n", "EVERY", 3);
    }
    
    /* 查询网络附着情况 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CGATT?\r\n", "EVERY", 30);
    
    /* 查询当前网络制式 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT$MYSYSINFO\r\n", "EVERY", 3);
    
    if((init_option == NORMAL_TCP_UNTRAN_CLIENT ) ||
       (init_option == NORMAL_TCP_UNTRAN_SERVICE) ||
       (init_option == NORMAL_TCP_TRAN_CLIENT)) {
        
        /* 设置APN */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CGDCONT=1,\"IP\",\"CMNET\"\r\n", "OK", 3);
        /* 设置身份认证参数 */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT+XGAUTH=1,1,\"gsm\",\"1234\"\r\n", "OK", 3);

    } else if ((init_option == NATION_TCP_UNTRAN_CLIENT ) ||
               (init_option == NATION_TCP_UNTRAN_SERVICE) ||
               (init_option == NATION_TCP_TRAN_CLIENT)) {
        /* 设置APN */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT$MYNETCON=0,\"APN\",\"CMNET\"\r\n", "OK", 3);
        /* 设置身份认证参数 */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT$MYNETCON=0,\"USERPWD\",\"CARD,CARD\"\r\n", "OK", 3)
        /* 设置身份认证参数 */
        SEND_COMMAND_WITH_RESTART(uart_handle, "AT$MYNETCON=0,\"AUTH\",1\r\n", "OK", 3);
    }
    return AM_OK;
}

/* 设置PPP拨号参数 */
void set_ppp_dial_parameters()
{
    /* 设置PPP拨号参数 */
    /*   ...   */
    /*   ...   */
    /*   ...   */
}

/* 进行PPP拨号 */
static int perform_ppp_dialing(am_uart_rngbuf_handle_t uart_handle)
{
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+XIIC=1\r\n", "EVERY", 3);
    
    return query_the_ppp_connection(uart_handle);
    
}

/* 查询PPP连接建立情况 */
static int query_the_ppp_connection(am_uart_rngbuf_handle_t uart_handle)
{
    static int error_times = 0;
    if(SEND_COMMAND(uart_handle, "AT+XIIC？\r\n", "+XIIC: 1", 30) != AM_OK) {
        error_times++;
        if(error_times == 4) {/* 重拨超过三次，重启模块 */
            RESTART_NEOWAY_N58;
            return AM_ERROR;
        }
        perform_ppp_dialing(uart_handle);/* 重拨 */
    }
    
    return close_server_listening(uart_handle, 1);
}

/* 关闭服务器侦听 */
int close_server_listening(am_uart_rngbuf_handle_t uart_handle, int establish)
{
    int i = 3;
    while(i--) {
        int send_data_num = am_uart_rngbuf_send(uart_handle, 
                                               (uint8_t *)"AT+CLOSELISTEN\r\n", 
                                                sizeof("AT+CLOSELISTEN\r\n"));
        if(send_data_num == sizeof("AT+CLOSELISTEN\r\n")) {
            char recv_buff[64] = {0};
            /* 超时时间20S */
            for(int wait = 20; wait > 0; wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 64);
                
                if(recv_buff[0] != 0) {
                    break;
                }
                
            }
            if((strstr(recv_buff,"local link closed") != NULL) ||
               (strstr(recv_buff,"ERROR") != NULL)) {
               if(establish == 1){
                    establish_server_listening(uart_handle);
               } else {
                   attached_to_the_network(uart_handle);
               }
               return AM_OK;
            }
            
        }
    }
    RESTART_NEOWAY_N58;
    return AM_ERROR;
}


/* 建立服务器端侦听 */
int establish_server_listening(am_uart_rngbuf_handle_t uart_handle)
{
    char send_buff[32] = {0};
    snprintf(send_buff, 32, "AT+TCPLISTEN=%d\r\n", TCPLISTEN_PORT);
    SEND_COMMAND_WITH_RESTART(uart_handle, send_buff, "+TCPLISTEN: 0,OK/Listening", 3);
    
    
    return waiting_receiving_client_connection(uart_handle);
}

/* 等待接收客户端连接请求 */
static int waiting_receiving_client_connection(am_uart_rngbuf_handle_t uart_handle)
{
    /**
     * 需要设置等待客户端连接的超时时间，若超时时间到，
     * 没有收到任 何客户端连接请求，则主动重新拨号建立侦听。
     */
    int over_time = TCPLISTEN_OVER_TIME;
    
    while(over_time--) {
        char recv_buff[64] = {0};
        am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 64);
        if(strstr(recv_buff,"=") != NULL) {
            char *Y_start = strstr(recv_buff,"=");
            Y_start++;
            char *Y_end = strstr(recv_buff,",");
            *Y_end = 0;
            AcceptSocket = strtoul(Y_start, &Y_end, 10);
            return query_link_status(uart_handle);
        }
    }
    perform_ppp_dialing(uart_handle);
    
}

/* 查询链路状态 */
static int query_link_status(am_uart_rngbuf_handle_t uart_handle)
{
    static int error_times = 0;
    char send_buff[32] = {0};
    snprintf(send_buff, 32, "AT+CLIENTSTATUS=%d\r\n", AcceptSocket);
    int i = 3;
    while(i--) {
        int send_data_num = am_uart_rngbuf_send(uart_handle, 
                                               (uint8_t *)send_buff, 
                                                strlen(send_buff));
        if(send_data_num == strlen(send_buff)) {
            char recv_buff[64] = {0};
            am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 64);
            if(strstr(recv_buff,"CONNECT") != NULL) {
                char *buffer_start = strstr(recv_buff,"TCP");
                buffer_start+=4;
                char *buffer_end = buffer_start;

                while (*buffer_end != '\0') {
                    if ((*buffer_end < '0') || (*buffer_end > '9')) {
                        *buffer_end = '\0';
                        break;
                    }
                    buffer_end++;
                }
                
                if(strtoul(buffer_start, &buffer_end, 10) != 61440) {
                    error_times++;
                    if(error_times >= 5) {
                        establish_server_listening(uart_handle);
                    }
                } else {
                    /* 发送数据 */
                    return AM_OK;
                }
            } else if (strstr(recv_buff,"DISCONNECT") != NULL) {
                waiting_receiving_client_connection(uart_handle);
            }
        }
    }
    RESTART_NEOWAY_N58;
}

/* 发送数据 */
int neoway_send_data(am_uart_rngbuf_handle_t uart_handle, 
                            const uint8_t           *p_txbuf,
                            uint32_t                 nbytes)
{
    static int error_times = 0;
    char send_buff[32] = {0};
    
    snprintf(send_buff, 32, "AT+TCPSENDS=%d,%d\r\n", AcceptSocket, nbytes);
    am_uart_rngbuf_send(uart_handle, (uint8_t *)send_buff, strlen(send_buff));
    
    int i = 3;
    while (i--) {
        char recv_buff[32] = {0};
        am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 32);
        if(strstr(recv_buff,">") != NULL) {
            am_mdelay(75);
            am_uart_rngbuf_send(uart_handle, p_txbuf, nbytes);
            int wait = 30;
            while(wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 32);
                if(strstr(recv_buff,"+TCPSENDS") != NULL) {
                    if(strstr(recv_buff,"NOT ACTIVE") != NULL) {
                        /* 等待客户端 连接请求 */
                        AM_DBG_INFO("+TCPSENDS:SOCKET ID NOT ACTIVE\r\n");
                        waiting_receiving_client_connection(uart_handle);
                    } else {
                        return query_data_transmission(uart_handle);
                    }
                    break;
                }
            }
            
        }
    }
    RESTART_NEOWAY_N58;
}

/* 查询数据发送情况 */
static int query_data_transmission(am_uart_rngbuf_handle_t uart_handle)
{
    static int error_times = 0;
    char send_buff[32] = {0};
    snprintf(send_buff, 32, "AT+TCPACKS=%d\r\n", AcceptSocket);
    am_uart_rngbuf_send(uart_handle, (uint8_t *)send_buff, strlen(send_buff));
    
    int i = 5;
    while (i--) {
        char recv_buff[32] = {0};
        am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 32);
        if(strstr(recv_buff,"+TCPACK") != NULL) {
            if(strstr(recv_buff,"DISCONNECT") != NULL) {
                establish_server_listening(uart_handle);
            } else if(strstr(recv_buff,"Y,") != NULL) {
                char *num1_start = strstr(recv_buff,"Y,") + 2;
                
                char *num1_end = num1_start;
                while (*num1_end != '\0') {
                    if ((*num1_end < '0') || (*num1_end > '9')) {
                            *num1_end = '\0';
                            break;
                        }
                        num1_end++;
                }
                char *num2_start = num1_end + 1;
                
                char *num2_end = num2_start;
                while (*num2_end != '\0') {
                    if ((*num2_end < '0') || (*num2_end > '9')) {
                            *num2_end = '\0';
                            break;
                        }
                        num2_end++;
                }
                
                if(strtoul(num1_start, &num1_end, 10) == 
                   strtoul(num2_start, &num2_end, 10)) {
                   
                   AM_DBG_INFO("send data success!\r\n");
                    /* 继续发送数据或者接收数据 */
                    /*           ...            */
                    /*           ...            */
                    /*           ...            */
                   return AM_OK;
                }
            } 
            
        } else {
            RESTART_NEOWAY_N58;
        }
    }
    query_the_ppp_connection(uart_handle);
}

/* 去附着网络 */
int attached_to_the_network(am_uart_rngbuf_handle_t uart_handle)
{
    int i = 3;
    while(i--) {
        int send_data_num = am_uart_rngbuf_send(uart_handle, 
                                               (uint8_t *)"AT+CGATT=0\r\n", 
                                                sizeof("AT+CGATT=0\r\n"));
        if(send_data_num == sizeof("AT+CGATT=0\r\n")) {
            char recv_buff[64] = {0};
            /* 超时时间20S */
            for(int wait = 20; wait > 0; wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 64);
                
                if(recv_buff[0] != 0) {
                    break;
                }
                
            }
            if((strstr(recv_buff,"OK") != NULL) ||
               (strstr(recv_buff,"ERROR") != NULL)) {
//                establish_server_listening(uart_handle);
                   /* 关机 */
                   return AM_OK;
            }
            
        }
    }
    RESTART_NEOWAY_N58;
}

/**
 * \brief  标准 TCP 非透传服务器端
 */
int am_normal_tcp_untran_service(am_uart_rngbuf_handle_t uart_handle)
{
    /* 模块初始化流程0 */
    am_neoway_init(uart_handle, NEOWAY_4G_MODULE, NORMAL_TCP_UNTRAN_SERVICE);
    
//    set_ppp_dial_parameters(uart_handle);
    
    perform_ppp_dialing(uart_handle);
}




/*******************************************************************************
    two
*******************************************************************************/
/* 使用P23例程应填充 */
int resend_data(am_uart_rngbuf_handle_t uart_handle)
{
    //P23收发数据
    
    
    
}


/* 发送数据 */
static int tcp_send_data(am_uart_rngbuf_handle_t uart_handle,
                        char                    *send_data, 
                        uint8_t                  socket_id  )
{
    char send_command[25]; 
    uint8_t recv_buff[50] = {0};
    int repeat_num = 3;
    snprintf(send_command, 25, "AT+TCPSEND=%d,%d\r\n", socket_id, strlen(send_data));
    
    while(repeat_num--) {
        am_uart_rngbuf_send(uart_handle, (uint8_t*)send_command, strlen(send_command));
        for(int wait = 30; wait > 0; wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 50);
                
                if(recv_buff[0] != 0) {
                    break;
                }
        }
        
        if(strstr((const char *)recv_buff,">") != NULL) {
            am_mdelay(80);
            int send_data_num = am_uart_rngbuf_send(uart_handle, (uint8_t *)send_data, strlen(send_data));
            
            if(send_data_num == strlen(send_data)) {
                am_uart_rngbuf_receive(uart_handle, recv_buff, 40);
                
                if (strstr((const char *)recv_buff,"+TCPSEND: SOCKET ID OPEN FAILED") != NULL){
                    return AM_ERROR;
                } else if(strstr((const char *)recv_buff,"+TCPSEND：") != NULL) {
                    return AM_OK;
                } 
            }
        }
        
    }
    return AM_ERROR;
}

/* 查询数据发送情况 */
static int tcp_send_data_status(am_uart_rngbuf_handle_t uart_handle, uint8_t socket_id)
{
    char send_command[20];
    if(socket_id == 0){
        snprintf(send_command, 20, "AT+TCPACKS\r\n");
    } else {
        snprintf(send_command, 20, "AT+TCPACKS=%d\r\n", socket_id);
    }
    char *buffer_end = NULL;
    uint8_t recv_buff[40] = {0};
    char *data_buff1;
    char *data_buff2;
    int repeat_num = 5;
    while(repeat_num--) {
        am_uart_rngbuf_send(uart_handle, (uint8_t*)send_command, strlen(send_command));
        
        for(int wait = 30; wait > 0; wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 40);
                
                if(recv_buff[0] != 0) {
                    break;
                }
        }
        if (recv_buff[0] == 0) {
            RESTART_NEOWAY_N58;
            return AM_ERROR;
        }
        if(socket_id != 0) {
            if (strstr((const char *)recv_buff,"+TCPACK: 0,DISCONNECT")) {
                return AM_ERROR;
            } else if(strstr((const char *)recv_buff,"+TCPACK:") != NULL) {
                data_buff1 = strstr((const char *)recv_buff,",")+1;
                data_buff2 = strstr((const char *)data_buff1,",")+1;
                *(data_buff2 - 1) = '\0';
                for (int i = 40; i >= 0; i--){
                    if(recv_buff[i] == '\r') {
                        recv_buff[i] = '\0';
                        break;
                    }
                }
                
                if(strtoul(data_buff1, &buffer_end, 10) == strtoul(data_buff2, &buffer_end, 10)) {
                    return AM_OK;
                } else {
                    continue;
                }
            }  else {
                RESTART_NEOWAY_N58;
                return AM_ERROR;
            }
        } else {
            if (strstr((const char *)recv_buff,"+TCPACK: DISCONNECT")) {
                return AM_ERROR;
            } else if(strstr((const char *)recv_buff,"+TCPACK:") != NULL) {
                data_buff1 = strstr((const char *)recv_buff,":")+1;
                data_buff2 = strstr((const char *)data_buff1,",")+1;
                *(data_buff2 - 1) = '\0';
                for (int i = 40; i >= 0; i--){
                    if(recv_buff[i] == '\r') {
                        recv_buff[i] = '\0';
                        break;
                    }
                }
                
                if(strtoul(data_buff1, &buffer_end, 10) == strtoul(data_buff2, &buffer_end, 10)) {
                    return AM_OK;
                } else {
                    continue;
                }
            }  else {
                RESTART_NEOWAY_N58;
                return AM_ERROR;
            }
        }
    }
    return AM_ERROR;
}

/*
 * uart_handle    : 具有环形缓冲区的串口操作句柄，串口超时1s
 * *send_data     : 需要发送的字符串
 * *recv_data    : 对接收的字符串进行判断
 *  repeat_num   : 重复次数，
 *  wait_time    ：接收等待时间（单位s）
 */
static int send_recv_with_decide2(am_uart_rngbuf_handle_t uart_handle,
                                 uint8_t                *send_data, 
                                 uint8_t                *recv_data, 
                                 int                     repeat_num,
                                 int                     wait_time )
{
    uint8_t null_times = 0;
    while(repeat_num--) {
        
        int send_data_num = am_uart_rngbuf_send(uart_handle, send_data, strlen((char*)send_data));
        
        if(send_data_num == strlen((char*)send_data)) {
            uint8_t recv_buff[64] = {0};
            for(int wait = wait_time; wait > 0; wait--) {
                am_uart_rngbuf_receive(uart_handle, (uint8_t *)recv_buff, 64);
                
                if(recv_buff[0] != 0) {
                    break;
                }
                
            }
            
            if(recv_buff[0] == 0) {
                null_times++;
                if(null_times == 3) {
                    return AM_ERROR;
                }
            }
            
            if(strstr((const char *)recv_data,"EVERY") != NULL) {
                if(recv_buff[0] != 0) {
                    return AM_OK;
                }
            }  else if(strstr((const char *)recv_data,"/") != NULL) {
                char *recv_data0 = (char *)recv_data;
                char *recv_data1 = strstr((const char *)recv_data,"/") + 1;
                *(recv_data1 - 1) = 0;
                
                if((strstr((const char *)recv_buff, recv_data0) != NULL) || 
                   (strstr((const char *)recv_buff, recv_data1) != NULL)) {
                    return AM_OK;
                }
            } else if(strstr((const char *)recv_buff, (const char *)recv_data) != NULL) {
                return AM_OK;
            } 
              
             else if(strstr((const char *)recv_buff, "DISCONNECT") != NULL) {
                return AM_ERROR;
            }
            
        } else {
            return AM_ERROR;
        }
    }
    return AM_ERROR;
}




/* P10 */
int am_neoway_n58_tcp_communicate(am_uart_rngbuf_handle_t uart_handle, uint8_t socket_id, char *send_data)
{
    uint8_t tcp_link = 0x01;    /* 用于定义状态 */
    uint8_t next_brand = 0;     /* 控制拨号 */
    char tx_command[40];
    char rx_command[40];
    
    while(1) {
        switch(tcp_link) {
            case 0x01 : 
                uart_handle->timeout_ms = 1000;
                /* 设置PPP拨号参数 */
                set_ppp_dial_parameters();
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+XIIC=1\r\n", (uint8_t*)"OK", 3, 1);
                tcp_link = 0x02;
                break;
            case 0x02 :                                                 //4
                if(send_recv_with_decide2(uart_handle, (uint8_t*)"AT+XIIC？\r\n", (uint8_t*)"+XIIC： 1", 30, 1) != AM_OK) {
                    tcp_link = 0x01;             /* 拨号失败从新拨号 */
                    next_brand++;
                    if(next_brand > 3) {
                        RESTART_NEOWAY_N58;
                        next_brand = 0;
                        return AM_ERROR;
                    }
                    break;
                }
                next_brand = 0;
                snprintf(tx_command, 20, "AT+TCPCLOSE=%d\r\n", socket_id);
                snprintf(rx_command, 20, "TCPCLOSE: %d", socket_id);
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)tx_command, (uint8_t*)rx_command, 3, 30);
                
                snprintf(tx_command, 40, "AT+TCPSETUP=%d,%s,%d\r\n", socket_id, TCP_IP, TCPLISTEN_PORT);
                snprintf(rx_command, 20, "+TCPSETUP: %d,OK", socket_id);
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)tx_command, (uint8_t*)rx_command, 3, 30);
                
                tcp_link = 0x04;
                break;
            case 0x04 :                                                 //7
                snprintf(tx_command, 20, "AT+IPSTATUS=%d\r\n", socket_id);
                snprintf(rx_command, 40, "+IPSTATUS: %d,CONNECT,TCP,4096", socket_id);
                if(send_recv_with_decide2(uart_handle, (uint8_t*)tx_command, (uint8_t*)rx_command, 5, 1) != AM_OK) {
                    tcp_link = 0x02;
                    break;
                }
                if(tcp_send_data(uart_handle, send_data, socket_id) != AM_OK) {
                    tcp_link = 0x02;
                    break;
                }
                tcp_link = 0x08;
                break;
            case 0x08 :                                                 //9
                if(tcp_send_data_status(uart_handle, socket_id) != AM_OK) {
                    tcp_link = 0x02;
                    break;
                }
                snprintf(tx_command, 20, "AT+TCPCLOSE=%d\r\n", socket_id);
                snprintf(rx_command, 20, "TCPCLOSE: %d", socket_id);
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)tx_command, (uint8_t*)rx_command, 3, 30);
                
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+CGATT=0\r\n", (uint8_t*)"OK/ERROR", 3, 15);
                return AM_OK;
        }
    }
}

/* P23 */
int am_neoway_n58_tcp_Penetrate(am_uart_rngbuf_handle_t uart_handle)
{
    uint8_t tcp_link = 0x01;
    uint8_t next_brand = 0;
    char tx_command[20];
    char rx_command[40];
    
    
    while(1) {
        switch(tcp_link) {
            case 0x01 : 
                uart_handle->timeout_ms = 1000;
                /* 设置PPP拨号参数 */
                set_ppp_dial_parameters();
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+XIIC=1\r\n", (uint8_t*)"OK", 3, 1);
                tcp_link = 0x02;
                break;
            case 0x02 :                                                 //4
                if(send_recv_with_decide2(uart_handle, (uint8_t*)"AT+XIIC？\r\n", (uint8_t*)"+XIIC： 1", 30, 1) != AM_OK) {
                    tcp_link = 0x01;
                    next_brand++;
                    if(next_brand > 3) {
                        RESTART_NEOWAY_N58;
                        next_brand = 0;
                        return AM_ERROR;
                    }
                    break;
                }
                next_brand = 0;
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+TRANSCLOSE\r\n", (uint8_t*)"TCPCLOSE: ", 3, 30);
                
                snprintf(tx_command, 20, "AT+TCPTRANS=%s,%d\r\n", TCP_IP, TCPLISTEN_PORT);
                snprintf(rx_command, 20, "+TCPSETUP: OK");
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)tx_command, (uint8_t*)rx_command, 3, 30);
                tcp_link = 0x04;
                break;
            case 0x04 :                                                      //7
                //收发数据
                resend_data(uart_handle);
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"Send:+++\r\n", (uint8_t*)"OK", 3, 10);
                tcp_link = 0x08;
                break;
            case 0x08 :                                                 //8
                if(tcp_send_data_status(uart_handle, 0) != AM_OK) {
                    tcp_link = 0x02;
                    break;
                }
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+TRANSCLOSE\r\n", (uint8_t*)"TCPCLOSE: ", 3, 30);
                SEND_COMMAND_TWO(uart_handle, (uint8_t*)"AT+CGATT=0\r\n", (uint8_t*)"OK/ERROR", 3, 15);
                return AM_OK;
        }
    }
}

/* 外部协议栈 TCP 应用 */
int enternal_tcp_app_init(am_uart_rngbuf_handle_t uart_handle)
{
    am_neoway_init(uart_handle, NEOWAY_4G_MODULE, ENTERNAL_TCP_APP);
    
    /* 设置APN */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+CGDCONT=1，\"IP\",\"CMNET\"\r\n", "OK", 3);
    
    /* 设置身份认证参数 */
    SEND_COMMAND_WITH_RESTART(uart_handle, "AT+XGAUTH=1,1,\"gsm\",\"1234”\"\r\n", "OK", 3);
    
    return AM_OK;
    
}
/* 进入外部协议栈 */
int enter_external_protocol_stack(am_uart_rngbuf_handle_t uart_handle)
{
    if(enternal_tcp_app_init(uart_handle) == AM_OK) {
        SEND_COMMAND_WITH_RESTART(uart_handle, "ATD*99#\r\n", "CONNECT", 5);
    }
}




