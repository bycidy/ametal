/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/
/**
 * \file
 * \brief 低功耗模式（睡眠、深度睡眠）例程，通过驱动层接口实现
 *
 * - 实现现象
 *   1.串口提示当前正在进行的模式测试，首先进入睡眠模式。
 *   2.按下 SW1键（PIOA_0）唤醒 MCU,并进入深度睡眠模式测试。
 *   3.按下 SW1键（PIOA_0）唤醒 MCU,LED闪烁，MCU处于正常工作模式。
 *
 * \note
 *   1.测试本例程之前应将 am_prj_config.h 中的宏 AM_CFG_SYSTEM_TICK_ENABLE、
 *     AM_CFG_SOFTIMER_ENABLE 和   AM_CFG_KEY_GPIO_ENABLE 设置为 0。
 *   2.如需观察串口打印的调试信息，需要将 PIOB_11 引脚连接 PC 串口的 TXD，
       PIOB_12 引脚连接 PC 串口的 RXD。
 *
 * \par 源代码
 * \snippet demo_zsl42x_drv_lpmode_wake_up.c src_zsl42x_drv_lpmode_wake_up
 *
 * \internal
 * \par Modification History
 * - 1.00 19-10-15  zp, first implementation
 * \endinternal
 */

/**
 * \addtogroup demo_if_zsl42x_drv_lpmode_wake_up
 * \copydoc demo_zsl42x_drv_lpmode_wake_up.c
 */

/** [src_zsl42x_drv_lpmode_wake_up] */
#include "ametal.h"
#include "am_board.h"
#include "am_led.h"
#include "am_delay.h"
#include "am_gpio.h"
#include "am_vdebug.h"
#include "zsl42x_pin.h"
#include "am_zsl42x_inst_init.h"
#include "demo_am42x_core_entries.h"
#include "am_zsl42x_lpmode.h"

/**
 * \brief 引脚中断服务函数
 */
static void __gpio_isr (void *p_arg)
{
    ;
}

void low_power_gpio_cfg()
{
    am_gpio_pin_cfg(PIOA_0,PIOA_0_INPUT_PD);
    am_gpio_pin_cfg(PIOA_1,PIOA_1_INPUT_PD);
    am_gpio_pin_cfg(PIOA_2,PIOA_2_INPUT_PD);
    am_gpio_pin_cfg(PIOA_3,PIOA_3_INPUT_PD);
    am_gpio_pin_cfg(PIOA_4,PIOA_4_INPUT_PD);
    //am_gpio_pin_cfg(PIOA_7,PIOA_7_INPUT_PD);
    am_gpio_pin_cfg(PIOA_8,PIOA_8_INPUT_PD);
    am_gpio_pin_cfg(PIOA_9,PIOA_9_INPUT_PD);
    am_gpio_pin_cfg(PIOA_10,PIOA_10_INPUT_PD);
    am_gpio_pin_cfg(PIOA_10,PIOA_10_INPUT_PD);
    am_gpio_pin_cfg(PIOA_11,PIOA_11_INPUT_PD);
    am_gpio_pin_cfg(PIOA_12,PIOA_12_INPUT_PD);
    am_gpio_pin_cfg(PIOA_13,PIOA_13_INPUT_PD);
    am_gpio_pin_cfg(PIOA_14,PIOA_14_INPUT_PD);
    
    am_gpio_pin_cfg(PIOB_0,PIOB_0_INPUT_PD);
    am_gpio_pin_cfg(PIOB_1,PIOB_1_INPUT_PD);
    am_gpio_pin_cfg(PIOB_2,PIOB_2_INPUT_PD);
    am_gpio_pin_cfg(PIOB_3,PIOB_3_INPUT_PD);
    am_gpio_pin_cfg(PIOB_4,PIOB_4_INPUT_PD);
    am_gpio_pin_cfg(PIOB_5,PIOB_5_INPUT_PD);
    am_gpio_pin_cfg(PIOB_6,PIOB_6_INPUT_PD);
    am_gpio_pin_cfg(PIOB_7,PIOB_7_INPUT_PD);
    am_gpio_pin_cfg(PIOB_8,PIOB_8_INPUT_PD);
    am_gpio_pin_cfg(PIOB_9,PIOB_9_INPUT_PD);
    am_gpio_pin_cfg(PIOB_10,PIOB_10_INPUT_PD);
    am_gpio_pin_cfg(PIOB_11,PIOB_11_INPUT_PD);
    am_gpio_pin_cfg(PIOB_12,PIOB_12_INPUT_PD);
    am_gpio_pin_cfg(PIOB_13,PIOB_13_INPUT_PD);
    am_gpio_pin_cfg(PIOB_14,PIOB_14_INPUT_PD);
    am_gpio_pin_cfg(PIOB_15,PIOB_15_INPUT_PD);
    
    am_gpio_pin_cfg(PIOC_0,PIOC_0_INPUT_PD);
    am_gpio_pin_cfg(PIOC_1,PIOC_1_INPUT_PD);
    am_gpio_pin_cfg(PIOC_2,PIOC_2_INPUT_PD);
    am_gpio_pin_cfg(PIOC_3,PIOC_3_INPUT_PD);
    am_gpio_pin_cfg(PIOC_4,PIOC_4_INPUT_PD);
    am_gpio_pin_cfg(PIOC_5,PIOC_5_INPUT_PD);
    am_gpio_pin_cfg(PIOC_6,PIOC_6_INPUT_PD);
    am_gpio_pin_cfg(PIOC_7,PIOC_7_INPUT_PD);
    am_gpio_pin_cfg(PIOC_8,PIOC_8_INPUT_PD);
    am_gpio_pin_cfg(PIOC_9,PIOC_9_INPUT_PD);
    am_gpio_pin_cfg(PIOC_13,PIOC_13_INPUT_PD);
    am_gpio_pin_cfg(PIOC_15,PIOC_15_INPUT_PD);

    am_gpio_pin_cfg(PIOD_2,PIOD_2_INPUT_PD);
    
    am_gpio_pin_cfg(PIOF_0,PIOF_0_INPUT_PD);
    am_gpio_pin_cfg(PIOF_1,PIOF_1_INPUT_PD);
    am_gpio_pin_cfg(PIOF_7,PIOF_7_INPUT_PD);
}

/**
 * \brief 例程入口
 */
void demo_zsl42x_drv_lpmode_wake_up_entry (void)
{
    AM_DBG_INFO("sleep mode test!\r\n");
    am_mdelay(100);

    /* 引脚中断配置 */
    am_gpio_pin_cfg(PIOA_0, PIOA_0_INPUT_PU);
    am_gpio_trigger_connect(PIOA_0, __gpio_isr, NULL);
    am_gpio_trigger_cfg(PIOA_0, AM_GPIO_TRIGGER_FALL);
    am_gpio_trigger_on(PIOA_0);

    /* 不使用引脚配置 */
    low_power_gpio_cfg();
    
    /* 低功耗模式初始化 */
    am_zsl42x_lpmode_init();

    /* 睡眠模式下系统时钟配置 */
    am_zsl42x_lpmode_clk_change(AM_ZSL42x_LPMODE_MODE_SLEEP);

    /* 进入睡眠模式，唤醒后不再进入 */
    am_zsl42x_lpmode_sleep(AM_FALSE);

    AM_DBG_INFO("sleep mode, wake_up!\r\n");

    AM_DBG_INFO("deepsleep mode test!\r\n");

    /* 睡眠模式下系统时钟配置，函数内部将时钟切换为RCL，若不需要可屏蔽 */
    am_zsl42x_lpmode_clk_change(AM_ZSL42x_LPMODE_MODE_DEEPSLEEP);

    /* 进入深度睡眠模式，唤醒后不再进入 */
    am_zsl42x_lpmode_deepsleep(AM_FALSE);

    AM_DBG_INFO("deepsleep mode, wake_up!\r\n\r\n");

    while (1) {

        /* 指示灯指示时钟是否恢复正常 */
        am_led_on(LED0);
        am_mdelay(500);
        am_led_off(LED0);
        am_mdelay(500);
    }
}
/** [src_zsl42x_drv_lpmode_wake_up] */

/* end of file */
