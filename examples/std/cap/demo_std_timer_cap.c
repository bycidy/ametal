/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief 定时器实现捕获功能，通过标准接口实现
 *
 * - 操作步骤：
 *   1. 将 PWM 输出连接到捕获输入引脚。
 *
 * - 实验现象：
 *   1. 调试串口输出捕获到的 PWM 信号的周期和频率。
 *
 * \par 源代码
 * \snippet demo_std_timer_cap.c src_std_timer_cap
 *
 * \internal
 * \par Modification history
 * - 1.00 16-04-22  nwt, first implementation
 * \endinternal
 */ 

/**
 * \addtogroup demo_if_std_timer_cap
 * \copydoc demo_std_timer_cap.c
 */

/** [src_std_timer_cap] */
#include "ametal.h"
#include "am_cap.h"
#include "am_vdebug.h"
#include "am_delay.h"

static volatile uint32_t  __g_time_ns = 0;           /**< \brief 捕获计数值 */

/**
 * \brief 捕获回调函数
 */
void cap_callback (void *p_arg, unsigned int cap_val)
{
    static uint32_t cap_val_prev = (uint32_t)-1;

    if (cap_val > cap_val_prev) {
        am_cap_count_to_time((am_cap_handle_t)p_arg, 0, cap_val_prev, cap_val, (uint32_t *)&__g_time_ns);
    }
    cap_val_prev = cap_val;
}

/**
 * \brief 单次读取捕获频率
 */
int read_freq()
{
    int32_t freq = -1;//init as invalid value
    uint32_t time_ns = __g_time_ns;

    if (time_ns > 0) {
        freq = 1000000000 / time_ns;
    }

    return freq;
}

/**
 * \brief 例程入口
 */
void demo_std_timer_cap_entry (am_cap_handle_t cap_handle, int cap_chan)
{
    int32_t freq = 0;

    /* 捕获输入配置 */
    am_cap_config(cap_handle,
                  cap_chan,
                  AM_CAP_TRIGGER_RISE,
                  __cap_callback,
                  (void *)cap_handle);
    am_cap_enable(cap_handle, cap_chan);

    /* 循环读取捕获频率 */
    while (1) {
        freq = read_freq();
        AM_DBG_INFO("The period is %d ns, The freq is %d Hz \r\n", __g_time_ns, freq);
        am_mdelay(1000);
    }
}
/** [src_std_timer_cap] */

/* end of file */
