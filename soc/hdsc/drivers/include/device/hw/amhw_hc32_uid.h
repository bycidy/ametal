/*******************************************************************************
*                                 AMetal
*                       ----------------------------
*                       innovating embedded platform
*
* Copyright (c) 2001-2018 Guangzhou ZHIYUAN Electronics Co., Ltd.
* All rights reserved.
*
* Contact information:
* web site:    http://www.zlg.cn/
*******************************************************************************/

/**
 * \file
 * \brief UID操作接口
 *
 * \internal
 * \par Modification history
 * - 1.00 20-10-10
 * \endinternal
 */

#ifndef __AMHW_HC32_UID_H
#define __AMHW_HC32_UID_H

#ifdef __cplusplus
extern "C" {
#endif

#include "ametal.h"
#include "am_types.h"

/**
 * \brief 使用匿名联合体段开始
 * @{
 */

#if defined(__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined(__ICCARM__)
  #pragma language=extended
#elif defined(__GNUC__)

  /* 默认使能匿名联合体 */
#elif defined(__TMS470__)

  /* 默认使能匿名联合体 */
#elif defined(__TASKING__)
  #pragma warning 586
#else
  #warning Not supported compiler t
#endif

/** @} */
/**
 * \brief UID - 寄存器组
 */
typedef struct amhw_hc32_uid {
    uint16_t uid[5];        /**< \brief 产品唯一标识寄存器 (80 位) */     
} amhw_hc32_uid_t;



/**
 * \brief 获取UID
 *
 * \param[in] p_hw_device : 指向UID寄存器组基地址指针
 * \param[in] p_uid : 指向UID被存储的位置，可定义类型为uint16_t [5];
 * \retval : 无
 */
am_static_inline
void amhw_hc32_get_uid (amhw_hc32_uid_t *p_hw_uid, uint16_t *p_uid)
{
    p_uid[0] = (uint16_t)(p_hw_uid->uid[0]);
    p_uid[1] = (uint16_t)(p_hw_uid->uid[1]);
    p_uid[2] = (uint16_t)(p_hw_uid->uid[2]);
    p_uid[3] = (uint16_t)(p_hw_uid->uid[3]);
    p_uid[4] = (uint16_t)(p_hw_uid->uid[4]);
}

/**
 * \brief 使用匿名联合体段结束
 * @{
 */

#if defined(__CC_ARM)
  #pragma pop
#elif defined(__ICCARM__)

  /* 允许匿名联合体使能 */
#elif defined(__GNUC__)

  /* 默认使用匿名联合体 */
#elif defined(__TMS470__)

  /* 默认使用匿名联合体 */
#elif defined(__TASKING__)
  #pragma warning restore
#else
  #warning Not supported compiler t
#endif
/** @} */

/**
 * @}
 */


#ifdef __cplusplus
}
#endif

#endif /* __AMHW_HC32_UID_H */

/* end of file */



